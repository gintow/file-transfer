import time
import mqtt_worker
import bs_worker


mqtt = mqtt_worker.mqtt_worker('basestation', '150.254.128.131', 11883)
bs = None
try:
    bs = bs_worker.bs_worker('/dev/ttyACM0', 115200)
except:
    bs = bs_worker.bs_worker('/dev/ttyACM1', 115200)

def on_serial_data(resp_head, resp):
    print(resp_head)
    print(resp)
    mqtt.put_data('jmp/base_station/telemetry', resp.decode('utf-8'))

bs.set_on_data(on_serial_data)

mqtt.start()
bs.start()

bs.get_configuration()

while True:
    time.sleep(1)
    pass
