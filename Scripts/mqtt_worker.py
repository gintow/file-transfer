import threading
import queue
import time
import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("connected to broker: " + client._host)

class mqtt_worker:
    q = queue.Queue()
    should_run = False
    broker = ''
    port = 0
    client = None

    def __init__(self, cname, broker, port):
        self.broker = broker
        self.port = port
        self.client =mqtt.Client(cname)
        self.client.on_connect = on_connect
        self.client.on_message = self.on_message
        res=self.client.connect(self.broker, self.port)
        self.client.loop_start()

    def on_message(client,userdata, msg):
        print("Got some nessage. Shouldn't happened, we are not subscribed to any topic and do not have handler for messages!")

    def put_data(self, topic, data):
        obj = {'topic':topic, 'data':data}
        self.q.put(obj)

    def worker(self):
        while self.should_run:
            time.sleep(0.01)
            if(not self.q.empty() and self.client.is_connected()):
                obj = self.q.get()
                print("Publishing to topic: " + obj['topic'] + ", data: " + obj['data'])
                self.client.publish(obj['topic'], obj['data'])
            elif not self.q.empty():
                print("Broker not yet connected!")
                time.sleep(1)

    def start(self):
        self.should_run = True
        self.thread = threading.Thread(target=self.worker)
        self.thread.start()

    def stop(self):
        self.should_run = False
