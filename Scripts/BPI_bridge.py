from urllib import response
from pickle import FALSE

import socket
import os
import time
import serial
import logging
import paho.mqtt.client as mqtt
import json

from flask import Flask, request, jsonify


UDP_IP = "127.0.0.1"
UDP_PORT = 1664
sock = socket.socket(socket.AF_INET, # Internet
                    socket.SOCK_DGRAM) # UDP

s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
s.connect(('10.201.46.220',80))
IPAddr=s.getsockname()[0]
print(IPAddr)
client = mqtt.Client(client_id = IPAddr, clean_session=False)
client.connect("10.201.46.220",port=11883,keepalive=60)
client.loop_start()

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected MQTT disconnection. Will auto-reconnect")

client.on_disconnect = on_disconnect

connected = False
counter = 0
port = '/dev/ttyS3'
#port = 'COM2'
baud = 115200
anch_id =["" for i in range(10)]
id_idx = 0


try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client

http_client.HTTPConnection.debuglevel = 1

# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

ser = serial.Serial(port, baud)
ser.parity = "N"
ser.bytesize = 8
ser.timeout = 0.5

app = Flask(__name__)
@app.route('/trigger',methods = ['POST'])
def index():
    global trigger_end
    global message
    trigger_end = False
    keep_alive = str(os.getpid())+';'+str(time.time())
    sock.sendto(bytes(keep_alive,'utf8'), (UDP_IP, UDP_PORT))
    if request.headers['token'] != "7a94266d3331d60a6f64a9081dcdd9a9":
        return jsonify("{}"), 401

    if 'timestamp' in request.headers:
        ID = request.headers['timestamp']
        hex_ID=(hex(int(ID))[2:].zfill(16))

        if "check" in request.headers:
            hex_ID = "SA" + hex_ID
            #stringdata = hex_ID.decode('utf-8')
            startLocalization(hex_ID)
        else:
            #stringdata = hex_ID.decode('utf-8')
            hex_ID = "ID" + hex_ID
            startLocalization(hex_ID)

        #socket.send_string(hex_ID)


        if trigger_end == True:
            return jsonify("{OK}"), 200
        else:
            return jsonify("{Failure}"), 200

    else:
        return jsonify("{'Invalid POST'}"), 200

def getAnchorsID():
    last_id ="1"
    global anch_id
    global id_idx
    i=0
    try:
        os.chmod('/etc/circus/logs/comm_id.txt', 0o777)
        with open('/etc/circus/logs/comm_id.txt', "r") as outfile:
            while last_id!= '':
                last_id=outfile.readline()
                anch_id[i]=last_id
                print(anch_id[i])
                i=i+1
            id_idx=i
    except IOError:
        print ("cannot open anchors id file!")

def startLocalization(loc_id):
    global anch_id
    global id_idx
    global trigger_end
    print(loc_id)
    #message="ID"+str(loc_id)+"/"
    message=str(loc_id)+"/"
    for i in range(id_idx-1):
        message+= anch_id[i].rstrip()
        if i<id_idx-2:
            message+=","
    print(message)
    try:
        ser.flushInput()
        ser.write(bytes(message, 'utf-8')+ b'\x00')
        for i in range (4):
            recv_data = ser.readline()
            #print(recv_data)
            td_check=recv_data.decode("utf-8")
            if "TDOA_PROCESS_END" in td_check:
                trigger_end = True
                break
            elif len(td_check) > 1:
                topic_name = "uwb/anchors_in_process/00000000/raw"
                tag_uid = recv_data[3:11]
                topic_name=topic_name.replace("00000000",tag_uid.decode("utf-8")                                                                                                                                                      )
                #print(topic_name + ': ' + recv_data.decode('utf-8'))
                client.publish(topic_name, recv_data)
            else:
                break
            recv_data = ''
    except IOError:
        print ("cannot send start frame")


def startDiagnostic(loc_id):
    global anch_id
    global id_idx
    global trigger_end
    global counter
    topic_name = "uwb/anchors/00000000/raw"
    #topic_error = "uwb/errors/00000000/00000"
    print(loc_id)
    for j in range(1):
        for i in range(id_idx-1):
            message=str(loc_id)+"/"
            message+= anch_id[i].rstrip()
            #print(message)
            try:
                ser.flushInput()
                ser.write(bytes(message, 'utf-8')+ b'\x00')
                recv_data = ser.readline()

                if len(recv_data) > 39:
                    data = {}
                    data['timestamp'] = recv_data[12:28].decode()
                    data['main_anchor'] = recv_data[31:37].decode()
                    data['aux_anchor'] = recv_data[38:44].decode()
                    data['distance'] = recv_data[45:49].decode()
                    dec_dist=int(recv_data[45:49].decode(),16)
                    data['dist_dec'] = dec_dist
                    data['sts_ping'] = recv_data[50:58].decode()
                    data['sts_pong'] = recv_data[59:67].decode()
                    data['sts_pong2'] = recv_data[68:76].decode()
                    json_data = json.dumps(data)
                    tag_uid=recv_data[31:37]
                    topic_name=topic_name.replace("00000000",tag_uid.decode("utf                                                                                                                                                      -8"))
                    client.publish(topic_name, json_data)
                    #publish.single(topic_name, json_data, hostname="10.201.46.2                                                                                                                                                      20")
                    if dec_dist>20000:
                        topic_error = "uwb/errors/00000000/00000"
                        counter = counter + 1
                        error_uid=recv_data[38:44]
                        error_code=dec_dist
                        topic_error=topic_error.replace("00000000",error_uid.dec                                                                                                                                                      ode("utf-8"))
                        topic_error=topic_error.replace("00000",str(error_code))
                        client.publish(topic_error, counter)
                        #publish.single(topic_error, counter, hostname="10.201.4                                                                                                                                                      6.220")
                    trigger_end = True
                ser.flushInput()
            except IOError:
                print ("cannot send diagnostic frame")
            time.sleep(0.1)
def sendConfig(config_msg):
    global trigger_end
    try:
        ser.write(bytes(config_msg, 'utf-8')+ b'\x00')
        trigger_end = True
    except IOError:
        print ("cannot send start frame")

getAnchorsID()
app.run(host='0.0.0.0', port=8080)
