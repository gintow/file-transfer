import threading
import queue
import time
import serial
from enum import Enum

class bs_context(Enum):
    BS_CONTEXT_COMMAND = 0x0100

class bs_command(Enum):
    GET_CONFIG = 1

class bs_worker:
    serial_name = ''
    baudrate = 9600
    should_run = False
    thread = None
    serial_obj = None
    on_data = None

    def __init__(self, serial_name, baudrate= 9600):
        self.serial_name = serial_name
        self.baudrate = baudrate
        self.serial_obj = serial.Serial(self.serial_name, self.baudrate)

    def worker(self):
        if not self.serial_obj.is_open:
            print('Serial port ' + self.serial_obj.name + ' is not opened!')
            exit(-1)
            
        while self.should_run:
            time.sleep(0.01)
            
            if(self.serial_obj.in_waiting):
                resp_head = self.serial_obj.read(10)
                resp_context = int.from_bytes(resp_head[:2], 'little')
                resp_offset = int.from_bytes(resp_head[2:6], 'little')
                resp_length = int.from_bytes(resp_head[6:10], 'little')
                resp = self.serial_obj.read(resp_length)

                if( self.on_data != None):
                    self.on_data(resp_head, resp)

    def set_on_data(self, func):
        self.on_data = func

    def start(self):
        if not self.serial_obj.is_open:
            self.serial_obj.open()
        self.should_run = True
        self.thread = threading.Thread(target=self.worker)
        self.thread.start()

    def stop(self):
        self.serial_obj.close()
        self.should_run = False

    def send_generic_command(self, context, command, command_offset, command_length, command_data, command_data_length):
        command_array = bytearray()

        command_array.extend(context.value.to_bytes(2, 'little'))
        command_array.extend(command_offset.to_bytes(4, 'little'))
        command_array.extend((command_length + command_data_length).to_bytes(4, 'little'))
        command_array.extend(command.value.to_bytes(1, 'little'))

        if command_data_length>0:
            command_array.extend(command_data)

        self.serial_obj.write(command_array)

    def get_configuration(self):
        self.send_generic_command(bs_context.BS_CONTEXT_COMMAND,bs_command.GET_CONFIG, 0, 1, None, 0)